package com.example.sukhrana.istdayandroid;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public  void  buttonPressed(View view){
       EditText  x = (EditText)findViewById(R.id.nameTextBox);
       String n = x.getText().toString();


        Log.d("jenelle","hello");
        Intent i = new Intent(this, SecondScene.class);
        i.putExtra("Username",n);

        i.putExtra("message","hi this is second screen");
        startActivity(i);
    }
}
