package com.example.sukhrana.istdayandroid;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class SecondScene extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second_scene);
        Intent i = getIntent();
        String u = i.getStringExtra("Username");
        String m = i.getStringExtra("message");
        TextView s = (TextView)findViewById(R.id.secondTextView) ;
        s.setText(u);
        Log.d("name", u);
        Log.d("message", m);
    }
    public  void previousButton(View view){
        Log.d("back", "going back to first screen");
        Intent i = new Intent(this,MainActivity.class);
        startActivity(i);
    }
}
